type t;

/* properties */
[@bs.get] external endpoint: t => string = "endpoint";

[@bs.get]
external expirationTime: t => option(int) = "expirationTime";

[@bs.get] external options: t => SubscriptionOptions.t = "options";


/* methods */
[@bs.send]
external getKey: (t, string) => Js_typed_array.array_buffer = "getKey";

[@bs.send] external toJson: t => string = "toJson";

[@bs.send] external unsubscribe: t => Js.Promise.t(bool)
  = "unsubscribe";