# bs-push

This package closely wraps the
[Push API](https://developer.mozilla.org/en-US/docs/Web/API/Push_API). 
It will not include the Push API Service Worker Additions. Instead,
it will be a dependency of the forthcoming `bs-service-worker`. This binding
is currently at a very early stage of development.

## Installation
`npm install bs-push`

## Implemented
- [X] [EventInitDict](https://developer.mozilla.org/en-US/docs/Web/API/PushEvent/PushEvent)
- [X] [EventType](https://developer.mozilla.org/en-US/docs/Web/API/PushEvent/PushEvent)
- [X] [ExtendableEvent](https://developer.mozilla.org/en-US/docs/Web/API/ExtendableEvent)
- [X] [PushEvent](https://developer.mozilla.org/en-US/docs/Web/API/PushEvent/PushEvent)
- [X] [PushMessageData](https://developer.mozilla.org/en-US/docs/Web/API/PushMessageData)
- [X] [PushSubscription](https://developer.mozilla.org/en-US/docs/Web/API/PushSubscription)
- [X] [SubscriptionOptions](https://developer.mozilla.org/en-US/docs/Web/API/PushSubscription/options)

## Notes
### 'data
`'data` is a frequent type parameter. It represents the untyped data object
passed into the eventInitDict of the PushEvent constructor. 

### PushMessageData
There's currently no BuckleScript `Blob` type binding that I'm aware of, so
rather than find a workaround, I'm just leaving that one as an exercise for 
the reader. 

