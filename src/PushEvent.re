type _pushEvent('a, 'data);
type pushEvent_like('a, 'data)
  = ExtendableEvent.extendableEvent_like(_pushEvent('a, 'data));
type t('data) = pushEvent_like(Dom._baseClass, 'data);

module Private = {
  module Make = {
    [@bs.send]
    external withOptions: (string, EventInitDict.t('data))
      => t('data)
      = "make";

    [@bs.send] external withoutOptions: (string) => t('data) = "make";
  };
};


let make = (~options=?, ~type_: string): t('data) => {
  switch (options) {
  | None => Private.Make.withoutOptions(type_)
  | Some(o) => Private.Make.withOptions(type_, o)
  };
};

[@bs.get] external data: t('data) => option(PushMessageData.t('data)) = "data";